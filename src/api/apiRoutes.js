const apiRoutes = {
    SEARCH_URL          : `/search`,
    ARTIST_URL          : `/artists/:artistId`,
    ARTIST_ALBUMS_URL   : `/artists/:artistId/albums`,
}

const apiRoutesBuilder    = {
    getSearchUrl         : () => apiRoutes.SEARCH_URL,
    getArtistUrl         : artistId => apiRoutes.ARTIST_URL.replace(":artistId", artistId),
    getArtistAlbumsUrl   : artistId => apiRoutes.ARTIST_ALBUMS_URL.replace(":artistId", artistId),
}

export default apiRoutesBuilder;