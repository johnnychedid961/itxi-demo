import axios from 'axios';
import {message} from 'antd';

import auth from '../helpers/auth';
import C from '../helpers/constances';
import { delay } from '../helpers/functions';

const axiosITXI = axios.create({
    baseURL: 'https://api.spotify.com/v1'
});

//request interceptor
axiosITXI.interceptors.request.use(function (config) {
    // Do something before request is sent
    config.headers['Authorization'] = `Bearer ${auth.getAuthCookie()}`;
    return config;
  }, function (error) {
    message.error('Sorry something went wrong');
    return Promise.reject(error);
});

//response interceptor
axiosITXI.interceptors.response.use(response => {
    return response.data;
}, error => {
    console.log(error.response);
    // can test here for other status codes and handle default http errors
    switch(error.response.status) {
        case C.HTTP_ERROR_CODES.UNAUTHORIZED:
            message.error('Please login');
            delay(500).then(auth.signout);
        break;
        default:
            message.error('Sorry something went wrong');
        break;

    }
    return Promise.reject(error);
});

export default axiosITXI;