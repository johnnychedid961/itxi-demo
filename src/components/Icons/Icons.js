import React from 'react';
import Icon from '@ant-design/icons';

import { ReactComponent as SpotifySVG } from './spotify.svg';

export const SpotifyIcon = props => <Icon component={SpotifySVG} {...props} />
