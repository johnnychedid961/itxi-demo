import React from "react";
import { useHistory } from "react-router-dom";
import auth from "../../helpers/auth";
import { Button } from "antd";
import { setCookie } from "../../helpers/cookies";
import C from "../../helpers/constances";
import appRoutesBuilder from "../../routes/appRoutes";
import { SpotifyIcon } from "../../components/Icons/Icons";

import styles from './Login.module.css';

function Login() {
    let history     = useHistory();

    // // Use the location if the state can be kept in the login to redirect use to the screen he landed from
    // let location    = useLocation();
    // let { from } = location.state || { from: { pathname: '/' } };
    
    const params = new URLSearchParams(window.location.hash.replace('#', ''));
    if(params.has('access_token')) {
        setCookie(C.COOKIE_KEYS.AUTH_TOKEN, params.get('access_token'),  params.get('expires_in')/86400)
    }
    if(auth.isAuthenticated())  history.replace(appRoutesBuilder.getDefaultLandingPage())
    
    return (
        <div className={styles.container}>
            <Button onClick={auth.authenticate} size="large" icon={<SpotifyIcon />}>Login</Button>
        </div>
    )
}

export default Login;