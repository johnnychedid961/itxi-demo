import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Typography, Spin, Col, Row, Card, Empty, Divider, Button, Pagination } from 'antd';

import axiosITXI from '../../api/axios';
import apiRoutesBuilder from '../../api/apiRoutes';
import ImageLoader from '../../components/Icons/common/ImageLoader';
import C from '../../helpers/constances';

const { Title, Text }     = Typography;

const paginationStyles = {
    display         : "flex",
    justifyContent  : "center",
    margin          : 20
}
const PAGE_LIMIT = 12;
function Artist() {
    const [isLoading, setIsLoading]     = useState(false);
    const [artistName, setArtistName]   = useState('');
    const [albums, setAlbums]           = useState([]);
    const [total, setTotal]             = useState(0);
    const [page, setPage]             = useState(1);

    let { artistId } = useParams();


    useEffect( () => {
        setIsLoading(true);
        Promise.all([
            axiosITXI.get(apiRoutesBuilder.getArtistUrl(artistId)),
            axiosITXI.get(apiRoutesBuilder.getArtistAlbumsUrl(artistId), {params: {limit: PAGE_LIMIT}})
        ])
        .then( r => {
            const [artist, artistAlbums] = r;
            
            setIsLoading(false);
            setArtistName(artist.name);
            setAlbums(artistAlbums.items);
            setTotal(artistAlbums.total);
        })
        .catch( e => setIsLoading(false))
    }, [artistId]);

    const getAlbumImage = (albumImagesArray, albumName) => 
    albumImagesArray.length > 1 
        ? <ImageLoader alt={albumName} src={albumImagesArray[0].url}/>
        : <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description="No Image"/>;


    const onPageChange = page => {
        setIsLoading(true);
        setPage(page);
        axiosITXI.get(apiRoutesBuilder.getArtistAlbumsUrl(artistId), {params: {limit: PAGE_LIMIT, offset: (page-1) * PAGE_LIMIT}})
        .then ( r => {
            setAlbums(r.items);
            setIsLoading(false);
            window.scrollTo(0, 0);
        })
    }
    return ( 
        <div style={{margin: "0 12px"}}>
            <Spin spinning={isLoading} >
                <Title level={3} style={{marginTop: 24}}>{artistName}</Title>
                <Text type="secondary">Albums</Text>
                <Divider style={{margin: "12px 0" }}/>

                <Row gutter={[C.ROW_GUTTER, C.ROW_GUTTER]}>
                    {
                        albums.map( album => {
                            return (
                                <Col {...C.COL_SPAN} className="d-flex-important"  key={album.id}>
                                    <Card
                                        hoverable
                                        cover   ={ getAlbumImage(album.images, album.name)}
                                        actions ={[
                                            <Button href={album.external_urls.spotify} target="_blank" className="mt-auto" type="primary">Preview on Spotify</Button>
                                        ]}
                                        >
                                        <div className="d-flex flex-direction-column">
                                            <Title level={3}>{album.name}</Title>
                                            {
                                                album.artists.map( a => (
                                                    <Text className="d-block" type="secondary" key={a.id}>{a.name}</Text>
                                                ))
                                            }
                                            <br />
                                            <Text type="secondary">{album.release_date}</Text>
                                            <Text type="secondary">{album.total_tracks} tracks</Text><br />
                                        </div>
                                    </Card>
                                </Col>
                            )
                        })
                    }
                </Row>
                <Pagination 
                style           ={paginationStyles}
                current         ={page}
                defaultCurrent  ={1} 
                total           ={total} 
                pageSize        ={PAGE_LIMIT}
                onChange        ={onPageChange}
                showSizeChanger ={false}
                />
            </Spin>
        </div>
    )
}

export default Artist;