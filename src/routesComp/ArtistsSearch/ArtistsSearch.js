import React, { useState, useRef, useEffect } from 'react';
import { Link, useHistory, useLocation } from 'react-router-dom';
import { Input, Spin, Card, Typography, Rate, Row, Col, Empty } from 'antd';
import {gsap} from "gsap";

import { debounce, localiseNumber, roundToNearest5, addParamsToUrl } from '../../helpers/functions';
import axiosITXI from '../../api/axios';
import apiRoutesBuilder from '../../api/apiRoutes';
import ImageLoader from '../../components/Icons/common/ImageLoader';
import C from '../../helpers/constances';

import styles from './ArtistsSearch.module.css';
import appRoutesBuilder from '../../routes/appRoutes';

const { Title }     = Typography;
const { Search }    = Input;

const STARS_NUMBER = 5;

let searchContainerStyles = {}

function ArtistsSearch() {
    const history   = useHistory();
    const location  = useLocation();

    const searchContainerRef = useRef(null);
    const searchRef = useRef(null);
    
    const [isLoading, setIsLoading]   = useState(false);
    const [artists, setArtists]       = useState([]);
    const query = new URLSearchParams(location.search).get('q');
    
    useEffect( () => {
        if(query){
            searchContainerStyles = { ...searchContainerStyles, top: 30};
            setIsLoading(true);
            axiosITXI.get(apiRoutesBuilder.getSearchUrl(), {params:{
                    q   : query, 
                    type: 'artist'
                }})
            .then( r => {
                setIsLoading(false);
                setArtists(r.artists.items);
            })
            .catch( e => setIsLoading(false))
        }

    }, [query])


    const onSearch =  value => {
        if(!value) {
            setArtists([]);
            history.push({
                pathname: '',
                search: ''
            });
            return;
        };
        const params = {
            q   : value, 
            type: 'artist'
        }
        
        history.push({
            pathname: '',
            search: addParamsToUrl('', {q: value})
        });

        setIsLoading(true);
        axiosITXI.get(apiRoutesBuilder.getSearchUrl(), {params})
        .then( r => {
            setIsLoading(false);
            setArtists(r.artists.items);
        })
        .catch( e => setIsLoading(false))
    }

    const onChange = debounce(onSearch, 300)
  
    const getArtistImage = (artistImagesArray, artistName) => 
        artistImagesArray.length > 1 
            ? <ImageLoader alt={artistName} src={artistImagesArray[0].url}/>
            : <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description="No Image"/>;

    const getStarsRating = rating =>  roundToNearest5( rating * 5/100 * 10) / 10;

    const onSearchFocus = () => gsap.to(searchContainerRef.current, { top: 30 });

    const onSearchBlur = () => artists.length === 0 && gsap.to(searchContainerRef.current, { top: "50%" });

    

    return (        
        <div className={styles.artistSearchContainer}>
            <div className={styles.searchContainer} style={searchContainerStyles} ref={searchContainerRef}>
                <Search
                    ref         ={searchRef}
                    defaultValue={query}
                    placeholder ="Search for an Artist"
                    onSearch    ={onSearch}
                    onChange    ={(e) => onChange(e.target.value)}
                    onFocus     ={onSearchFocus}
                    onBlur      ={onSearchBlur}
                />
            </div>

            <Spin spinning={isLoading}>
                <Row gutter={[C.ROW_GUTTER, C.ROW_GUTTER]} className={styles.searchResultsContainer}>
                    {
                        artists.map( artist => {
                            return (
                                <Col {...C.COL_SPAN} className="d-flex-important"  key={artist.id}>
                                    <Link to={appRoutesBuilder.getArtistUrl(artist.id)}>
                                        <Card
                                            hoverable
                                            cover   ={ getArtistImage(artist.images, artist.name)}
                                            >
                                            <Title level={3}>{artist.name}</Title>
                                            <div>
                                                {localiseNumber(artist.followers.total)} followers
                                            </div>
                                            <Rate 
                                                allowHalf 
                                                disabled
                                                count={STARS_NUMBER}
                                                value={getStarsRating(artist.popularity)}
                                            />
                                        </Card>
                                    </Link>
                                </Col>
                            )
                        })
                    }
                </Row>
            </Spin>
        </div>
    )
}

export default ArtistsSearch;