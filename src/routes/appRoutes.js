//Url used in the router to determine where each route should go
const BASE_URL  = ``;
export const appRoutes = {
    BASE_URL            : `${BASE_URL}`,
    LOGIN_URL           : `${BASE_URL}/login`,
    AUTH_URL            : `${BASE_URL}/auth`,
    ARTISTS_SEARCH_URL  : `${BASE_URL}/artists-search`,
    ARTIST_URL          : `${BASE_URL}/artists/:artistId`,
}

const appRoutesBuilder    = {
    getDefaultLandingPage  : () => appRoutes.ARTISTS_SEARCH_URL,

    getBaseUrl             : () => appRoutes.BASE_URL,
    getLoginUrl            : () => appRoutes.LOGIN_URL,
    getAuthUrl             : () => appRoutes.AUTH_URL,
    getArtistsSearchUrl    : () => appRoutes.ARTISTS_SEARCH_URL,
    getArtistUrl           : artistId => appRoutes.ARTIST_URL.replace(':artistId', artistId),
}

export default appRoutesBuilder ;