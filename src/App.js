import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import { message } from 'antd';

import appRoutesBuilder, { appRoutes } from './routes/appRoutes';
import auth from './helpers/auth';

import Login          from './routesComp/Login/Login';
import ArtistsSearch  from './routesComp/ArtistsSearch/ArtistsSearch';
import Artist         from './routesComp/Artist/Artist';

message.config({
  top: 80,
  duration: 1,
  maxCount: 1
});

function App() {
  const getDefaultRedirect = () => auth.isAuthenticated() ? appRoutesBuilder.getDefaultLandingPage() : appRoutesBuilder.getLoginUrl();

  return (
    <Router>
      <Switch>
        <Route path={appRoutes.LOGIN_URL}><Login /></Route>

        <PrivateRoute path={appRoutes.ARTISTS_SEARCH_URL}><ArtistsSearch /></PrivateRoute>
        <PrivateRoute path={appRoutes.ARTIST_URL}><Artist /> </PrivateRoute>

        <Redirect to={{ pathname: getDefaultRedirect() }} />
    </Switch>
    </Router>
  );
}

function PrivateRoute({ children, ...rest }) {
  return (
    <Route
      {...rest}
      render={({ location }) =>
        auth.isAuthenticated() ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: appRoutesBuilder.getLoginUrl(),
              state: { from: location }
            }}
          />
        )
      }
    />
  );
}

export default App;
