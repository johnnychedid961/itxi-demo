const C = {
    SPOTIFY_CLIENT_ID: "5a0545820059481395efb05dc3ddc229",
    COOKIE_KEYS : {
        AUTH_TOKEN               : 'auth_token',
    },
    HTTP_ERROR_CODES             : {
        UNAUTHORIZED         : 401,
        FORBIDDEN            : 403,
        NOT_FOUND            : 404,
        NOT_ACCEPTABLE       : 406,
        INTERNAL_SERVER_ERROR: 500
    },
    FETCH_MODES: {
        SAME_ORIGIN   : 'same-origin',
        CORS          : 'cors',
        NO_CORS       : 'no-cors'
    },
    ROW_GUTTER: { xs: 8, sm: 16, md: 24 },
    COL_SPAN: { xs: 24, sm: 12, md: 8, lg: 6 }
}
export {C as default};
