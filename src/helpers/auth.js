
import C from './constances';
import { getCookie, deleteCookie } from './cookies';
import { addParamsToUrl } from './functions';
import appRoutesBuilder from '../routes/appRoutes';

const auth = {
    getAuthCookie() {
        return getCookie(C.COOKIE_KEYS.AUTH_TOKEN)
    },
    isAuthenticated() {
        return !!auth.getAuthCookie();
    } ,
    authenticate(cb) {
        //Can Figure out a solution to redirect to landed page before login uaing redirect URI query string and the authenticate param instead of the cb
        window.location.href= decodeURIComponent(addParamsToUrl(
            'https://accounts.spotify.com/authorize',
            {
                client_id       : C.SPOTIFY_CLIENT_ID,
                redirect_uri    : `${window.location.origin}${appRoutesBuilder.getLoginUrl()}`,
                response_type   : 'token'
            }
        ))
    },
    signout() {
        deleteCookie(C.COOKIE_KEYS.AUTH_TOKEN);
        window.location.reload();
    }
};

export default auth;