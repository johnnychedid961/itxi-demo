import C from './constances';
import { getCookie } from './cookies';

/**
 * Helper function that logs the item sent to it
 * @param {any} x 
 */
export const trace = x => {
    console.log(x);
    return x;
}

export const isReactEnvProd = () => process.env.NODE_ENV === 'production';

export const isLoggedIn = () => !!getCookie(C.COOKIE_KEYS.AUTH_TOKEN);
//
export const mandatory = () => { throw new Error('Missing parameter!'); }
export const delay = t =>  new Promise(resolve => setTimeout(resolve, t));

export const reduceQsParamArray = (qs, arr) => arr.reduce( (prev, cur, i) => i === 0 ? `${qs}=${cur}` : `${prev}&${qs}=${cur}`, '');

export const isObject         = obj => obj === Object(obj);
export const isEmptyObj       = obj => Object.keys(obj).length === 0;
export const isNotEmptyObj    = obj => !isEmptyObj(obj);
export const isUndefined      = obj => obj === undefined;
export const isNotUndefined   = obj => !isUndefined(obj);
export const isEmptyString    = str => str === '' ? true: false;
export const isNotEmptyString = str => !isEmptyString(str);

/** 
 * Add the keys of the params object as query strings to the url
 * @param {String} url 
 * @param {Object} params object that the keys will be the name of the query parameter ands their value will be the query parameters value
 * @return {String} `${url}?${query}` 
 */
export function addParamsToUrl(url, params){
    const 
        esc     = encodeURIComponent,
        query   = Object
                    .keys(params)
                    // .map(p => esc(p) + '=' + esc(params[p]))
                    .map( p => Array.isArray(params[p]) ? reduceQsParamArray(p, params[p].map(esc) ) : esc(p) + '=' + esc(params[p]) )
                    .filter (isNotEmptyString)
                    .join('&');
    return `${url}?${query}`;
}

/**
 * wrap a promise and add to it a cancel method
 * @param {Promise} promise 
 */
export const makeCancelable = (promise) => {
  let hasCanceled_ = false;

  const wrappedPromise = new Promise((resolve, reject) => {
    promise.then((val) =>
      hasCanceled_ ? reject({isCanceled: true}) : resolve(val)
    );
    promise.catch((error) =>
      hasCanceled_ ? reject({isCanceled: true}) : reject(error)
    );
  });

  return {
    promise: wrappedPromise,
    cancel() {
      hasCanceled_ = true;
    },
  };
};

/**
 * Checks if the agent(device) is mobile or not
 */
export function isMobile() {
    let isMobile = false;
    const userAgent = navigator.userAgent;
    if(userAgent != null){
        if(userAgent.match('.*BlackBerry.*') || userAgent.indexOf('Mobile') !== -1) {
            isMobile = true;
        } else if(userAgent.match('.*Android.*') || userAgent.indexOf('Mobile') !== -1) {
            isMobile = true;
        } else if(userAgent.match('.*iPhone.*') || userAgent.match('.*iPad.*') || userAgent.indexOf('Mobile') !== -1) {
            isMobile = true;
        } 
    }
    return isMobile;
}

/**
 * localise the number sent to the default local
 * @param {int} number The number to localise
 */
export const localiseNumber = number => isNumeric(number) ? Number(number).toLocaleString() : number;

//Parse/Stringify JSON wrappers
export const jsonStringify = obj => JSON.stringify(obj);
export const jsonParse = obj => JSON.parse(obj);
//
export const isArrayEmpty = arr => arr.length === 0 ? true : false;
export const isTrue = v => !!v === true 

//
export const replaceStringToken = (str, token, value) => str.replace(token, value); 

//
export const poll = (fn, interval) => {
    const ref = setInterval(fn, interval);
    return {
        stop: () => clearInterval(ref)
    }
}
/**
 * Returns a function, that, as long as it continues to be invoked, will not be triggered. The function will be called after it stops being called for
 * N milliseconds. If `immediate` is passed, trigger the function on the
 * @param  {[type]} func      [description]
 * @param  {[type]} wait      [description]
 * @param  {[type]} immediate [description]
 * @return {[type]}           [description]
 */
export const debounce = (func, wait, immediate) => {
    let timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};

export const findByField = (fieldName, value) => (item) => item[fieldName] === value;


//scroll to DOMNode
export const scrollTo = DOMNode => DOMNode.scrollIntoView({ behavior: "smooth" });

export const equals = (a, b) => a === b;
export const notEquals = (a, b) => !equals(a, b);

export const startTimer = (timeleft, display) => {
    var downloadTimer = setInterval(function(){
        timeleft--;
        display.textContent = timeleft;
        if(timeleft <= 0)
            clearInterval(downloadTimer);
    }, 1000);
}

/**
 * 
 * @param {int} n length of array
 * @return ex n = 2 result= [undefined, undefined]
 */
export const createArrayWithNullValues    = n => Array.apply(null, Array(n));
/**
 * 
 * @param {int} n length of array
 * @return ex n = 5 result= [0, 1, 2, 3, 4, 5]
 */
export const createArrayWithNumbers       = (n) => createArrayWithNullValues(n).map(function (x, i) { return i });

export const isNumeric = n => !isNaN(parseFloat(n)) && isFinite(n);

export const roundNumber = scale => num => {
    if(!("" + num).includes("e")) {
      return +(Math.round(num + "e+" + scale)  + "e-" + scale);
    } else {
      var arr = ("" + num).split("e");
      var sig = ""
      if(+arr[1] + scale > 0) {
        sig = "+";
      }
      return +(Math.round(+arr[0] + "e" + sig + (+arr[1] + scale)) + "e-" + scale);
    }
}
export const roundNumberTo1 = roundNumber(1);
export const roundNumberTo2 = roundNumber(2);

export const createMarkup = markup => ({__html: markup});

export const loopTimes = (x, fn) => [...Array(x)].map((_, i) => fn(i));

export const generateColor = (numOfSteps, step) => {
  // This function generates vibrant, "evenly spaced" colours
  var r, g, b;
  var h = step / numOfSteps;
  var i = ~~(h * 6);
  var f = h * 6 - i;
  var q = 1 - f;
  switch(i % 6){
      case 0: r = 1; g = f; b = 0; break;
      case 1: r = q; g = 1; b = 0; break;
      case 2: r = 0; g = 1; b = f; break;
      case 3: r = 0; g = q; b = 1; break;
      case 4: r = f; g = 0; b = 1; break;
      case 5: r = 1; g = 0; b = q; break;
      default: break;
  }
  var c = "#" + ("00" + (~ ~(r * 255)).toString(16)).slice(-2) + ("00" + (~ ~(g * 255)).toString(16)).slice(-2) + ("00" + (~ ~(b * 255)).toString(16)).slice(-2);
  return (c);
}

export const objectToFormData = function(obj, form, namespace) {
    
  var fd = form || new FormData();
  var formKey;
  
  for(var property in obj) {
    if(obj.hasOwnProperty(property)) {
      
      if(namespace) {
        formKey = namespace + '[' + property + ']';
      } else {
        formKey = property;
      }
     
      // if the property is an object, but not a File,
      // use recursivity.
      if(typeof obj[property] === 'object' && !(obj[property] instanceof File)) {
        
        objectToFormData(obj[property], fd, property);
        
      } else {
        
        // if it's a string or a File object
        fd.append(formKey, obj[property]);
      }
      
    }
  }
  
  return fd;
    
};

export const roundToNearest = nearest => value => Math.ceil( value / nearest ) * nearest; 
export const roundToNearest5 = roundToNearest(5);